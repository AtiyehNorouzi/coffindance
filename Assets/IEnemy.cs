﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public abstract class IEnemy : MonoBehaviour
{
    [SerializeField]
    protected Transform grabPosition;

    [SerializeField]
    protected Transform coffinPosition;

    [SerializeField]
    protected GameObject coffin;

    [SerializeField]
    protected AudioSource audioSource;

    [SerializeField]
    protected GameObject player;

    [SerializeField]
    protected NavMeshAgent movingCharacter;

    protected Vector3 target;

    protected DancerAnimation animation;

    protected void SetVolume(float distance, float dist, float multiplier)
    {
        if (distance > dist)
            audioSource.volume = 0;
        else
            audioSource.volume = 1 / dist * multiplier;
    }

    protected float GetDistance(Vector3 a, Vector3 b)
    {
        return Vector3.Distance(new Vector3(a.x, 0, a.z), new Vector3(b.x, 0, b.z));
    }

}
