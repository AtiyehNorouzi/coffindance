﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ChasingCharacter : IEnemy
{

    float distToPlayer;

    void Update()
    {
        target = new Vector3(player.transform.position.x, movingCharacter.transform.position.y, player.transform.position.z);
        movingCharacter.SetDestination(target);

        distToPlayer = Vector3.Distance(movingCharacter.transform.position, player.transform.position);
        SetVolume(distToPlayer, 12, 5);
    }
}
