﻿using System;
using UnityEngine;
using UnityEngine.AI;

class PathFollowing : IEnemy
{
    [SerializeField]
    private Transform startPosition;

    [SerializeField]
    private Transform endPosition;

    float distToPlayer;

    void Start()
    {
        animation = GetComponent<DancerAnimation>();
        target = new Vector3(endPosition.position.x, movingCharacter.transform.position.y, endPosition.position.z);
    }
    bool backToStart = true;
    bool chase = false;
    float throwTimer = 0;
    public void ThrowObject()
    {
        Vector3 originalPosition = player.transform.localPosition;
        player.transform.position = Vector3.Lerp(originalPosition, coffinPosition.position, throwTimer );
    }

    private void Update()
    {
        Debug.Log("state" + animation.GetState());
        distToPlayer = GetDistance(transform.position, player.transform.position);
        if (animation.GetState() == DancerAnimation.animationState.throwObject)
        {
            player.transform.parent = coffinPosition.transform;
            throwTimer += Time.deltaTime / 1.5f;
            ThrowObject();
            if (throwTimer >= 1)
            {
                player.transform.localPosition = Vector3.zero;
                player.transform.localRotation = Quaternion.identity;
                animation.SetState(DancerAnimation.animationState.idle);
            }
            return;

        }
        if (animation.GetState() == DancerAnimation.animationState.grab)
            return;
        if (distToPlayer < 0.5f && animation.GetState() != DancerAnimation.animationState.grab)
        {
            chase = false;
            player.transform.parent = grabPosition;
            player.transform.localPosition = Vector3.zero;
            player.transform.localRotation = Quaternion.identity;

            player.GetComponent<CharacterController>().enabled = false;
            player.GetComponent<Player>().enabled = false;


            animation.SetState(DancerAnimation.animationState.grab);
            return;
        }

        if (chase)
        {
            movingCharacter.angularSpeed = 50f;
            movingCharacter.speed = 10f;
        }

        if (distToPlayer < 8)
            movingCharacter.SetDestination(player.transform.position);
        else
            movingCharacter.SetDestination(target);

        if (GetDistance(transform.position, target) < 0.5f)
        {
            target = backToStart ? startPosition.position : endPosition.position;
            backToStart = !backToStart;
        }

        if (distToPlayer < 3 && coffin.transform.parent)
        {
            chase = true;
            animation.SetState(DancerAnimation.animationState.running);
            coffin.transform.SetParent(null);
            coffin.transform.GetChild(0).GetComponent<Rigidbody>().isKinematic = false;
            coffin.transform.GetChild(0).GetComponent<Rigidbody>().AddForce(Vector3.up * 10, ForceMode.Impulse);
            coffin.transform.GetChild(1).GetComponent<Rigidbody>().isKinematic = false;
            coffin.transform.GetChild(1).GetComponent<Rigidbody>().AddForce(Vector3.up * 5, ForceMode.Impulse);
        }

        SetVolume(distToPlayer, 10, 5);
    }




}

