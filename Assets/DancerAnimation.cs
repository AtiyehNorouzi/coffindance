﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DancerAnimation : MonoBehaviour
{
    private Animator animator;
    public enum animationState
    {
        idle,
        hiphop,
        runningDance,
        running,
        grab,
        throwObject,
        lift,
        pull

    }
    public animationState currentState;
    int state = 0;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        currentState = animationState.idle;
    }

    // Update is called once per frame
    void Update()
    {

        if (!isPlaying(animator, "Idle") && currentState == animationState.idle)
        {
            state++;
            currentState = (animationState)state;  
            SetBool((animationState)state);
        }
        else if (currentState == animationState.hiphop)
        {
            if (!isPlaying(animator, "HipHopDance"))
            {
                currentState = animationState.idle;
                animator.Play("Idle");
            }
            
        }
        else if (!isPlaying(animator, "RunningDance") && currentState == animationState.runningDance)
        {
            state = 0;
            animator.SetBool("State1", false);
            animator.SetBool("State2", false);
            currentState = animationState.idle;
            animator.Play("Idle");
        }
        else if (!isPlaying(animator, "Picking") && currentState == animationState.grab)
        {
            state = (int)animationState.throwObject;
            currentState = (animationState)state;
            SetBool((animationState)state);
        }

    }
    void SetBool(animationState state)
    {
        switch (state)
        {
            case animationState.hiphop:
                {
                    animator.SetBool("State1", true);
                    animator.Play("HipHopDance");
                    break;
                }
            case animationState.runningDance:
                {
                    animator.SetBool("State2", true);
                    animator.Play("RunningDance");
                    break;
                }
            case animationState.running:
                {
                    currentState = animationState.running;
                    animator.SetBool("chase", true);
                    animator.Play("Running");
                    break;
                }
            case animationState.grab:
                {
                    currentState = animationState.grab;
                    animator.SetBool("grab", true);
                    animator.Play("Picking");
        
                    break;
                }
            case animationState.throwObject:
                {
                    currentState = animationState.throwObject;
                    animator.SetBool("throw", true);
                    animator.Play("Throwing");
                    animator.SetBool("chase", false);
                    animator.SetBool("grab", false);
                    break;
                }
            default:
                {
                    animator.SetBool("State4", true);
                    break;
                }
        }
    }
    bool isPlaying(Animator anim, string stateName)
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName(stateName) &&
                anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f)
            return true;
        else
            return false;
    }

    public void SetState(animationState state)
    {
        SetBool(state);
    }

    public animationState GetState()
    {
        return currentState;
    }
}